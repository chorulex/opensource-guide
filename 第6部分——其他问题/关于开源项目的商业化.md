# 关于开源项目的商业化

怎么找到一个开源平台？

怎么找到自己适合自己的开源的项目？

怎么克隆项目并且运用到自己的项目上？

开源项目到底是基于什么开源协议对外开放？

当你看到这个章节的时候相信你对以上话题有了一定的认知和了解	

### 开源和商业化的理解

​在众多的开源平台中，看到的形形色色的大型开源项目中，我们会留意到开源项目都注明各种各样的开源协议，比如 GPL、LGPL、BSD、MIT、Apache、Creative Commons 等等。

​疑问来了，不是说好的开源吗？怎么又冒出来个协议？难道我都获得了源代码了还不能用于我的项目之上吗？作者还要收费不成？没错是有这么个协议存在，而且具有一定的约束能力。我们所理解的开源有一点点和实际的开源存在错误的理解，所以必须把开源协议拿出来修饰一下这个话题。


> 开源许可协议：开源许可协议是指开源社区为了维护作者和贡献者的合法权利，保证软件不被一些商业机构或个人窃取，影响软件的发展而开发的协议。（来源：[百度百科](http://https://baike.baidu.com/item/%E5%BC%80%E6%BA%90%E8%AE%B8%E5%8F%AF%E5%8D%8F%E8%AE%AE/2470967?fr=aladdin)）

​不同的开源协议有着不同的开源约束能力，是为了保障开源项目的可持续性发展，只要我们在约束条件内，可以自由发挥，并非完全不能使用开源项目，使用者可以最大程度去复制、分发、盈利、修改、发行。

​开源是开源项目的核心，同时优秀的开源项目也承载着巨大的流量，当开源项目具备比较高的变现能力和其它核心竞争力的时候，项目可能会被收购，比如 MySQL，开源项目就有可能转化为商业项目。当开源项目的性质发生变化之后，可能会收回部分开源的核心功能，为了增加商业项目的竞争力量。

​开源的精神本质就是无偿贡献，但是在奉献过程中会消耗作者岁月、经历、资金。任何开源项目都有转化为商业的权力，这也是开源贡献者的权利。但是开源的商业转化将会丢失部分流量，这个时候贡献者要做到权衡利弊。

​既然不能直接转化商业项目获利，怎么又能保证贡献者获得相应的报酬呢？所以聪明的人类想到了一个折中的办法，那就是开源和商业化并存、相互依赖、商业服务叠加。一方面保证了流量的不丢失，一方面保证了开源商业化不受到阻碍。比如开源 Red Hat 是基于著名开源 Linux 系统内核开发的操作系统，本身 Red Hat 是开源免费的，源代码公开。但是系统中就叠加了商业收费服务，这些收费服务并不影响用户的使用，只有用户需要更加专业的、更高要求的服务才收费，这也保证了开源项目的最大商业化，社区成员也会高度重视这种叠加的收费服务。

开源和商业化并不冲突，而是相互共存、互补、突现。



### 商业化的开源项目特征


```
本段内容可进行修改补充
```


​在互联网的高速发展时代，让开源项目变成了可能，很多开源项目已经在开源的环境下实现了价值，开源是每一个个体和组织都可以贡献的一种资源。据相关数据表明近年以来中国的开源贡献以每年 37% 的增速在增长，国家也在高度重视开源精神、鼓励开源生态的发展。当下国内开源生态并不是很乐观，这也凸显了国内开源发展还有很大的空间，我们需要去创新、去发现、去贡献，每个开源贡献者都在微不足道地影响着开源生态发展，让国内开源生态更加的完善。

​经过了这几年的开源生态的认知和了解，每一个开源项目都承载着每个领域的发展推进，尤其是一些比较突出开源项目，都给用户带来了无限商业机会，也证实了开源项目对市场的影响力。开源是以最大的可能让商机发现自我，也许你就是那个最耀眼的星星，是你让用户的不可能变成了可能。

​每一个开源项目的贡献者都应该得到支持和鼓励，当开源项目的受众用户比较大，能为用户解决更多的问题，项目初期并不会用太多的用户来围观和反馈，流量也是比较少的。这个时期是贡献者们最头疼的问题，为什么我的项目没有用户采纳，这个要从两个方面去思考问题，一方面是项目没有被发现，另一方面是项目没有解决到实际问题，变成僵尸项目。开源项目的重复度如果比较高，也会造成开源项目的受众度低，所以创新很重要。每个开源贡献者都不希望自己的创新被白嫖，就算有个反馈意见也可以，并非一定要用户打赏以资鼓励才觉得这个项目是有意义的，只是希望开源的路上不再孤独。

​开源项目进化到商业项目是需要一个比较长的周期，也需要更多的用户来使用和共同维护，不断去积累用户，给用户解决能力之下非商业的问题，让用户产生依赖，增加项目粘度，同时也提高了项目知名度。当项目积累到了足够多的使用者，建立好良好的开源社区、问题处理的响应能力。这个时候就可以考虑开源项目的商业叠服务，为用户解决更多的可能，项目贡献者也可以获得前阶段无私贡献的回报，以良好的开源方向去服务好对项目认可的用户。

​不管你的开源项目是什么，都要明确项目的边界，也许你服务的是一个行业，也许是一个认知的市场，都要体现出开源项目的专业度。开源项目能不能商业化有几个方面的影响：能解决多少问题、是否必须、用户群体的大小、用户认可度、项目贡献者心态。开源项目不可能快速变现，初期要足够去创新更多的可能，等待用户发现自己。项目解决的问题越多，受众的用户就越大，是否必须决定了项目商业化的依赖程度，贡献者的心态决定了项目的存活周期，开源项目获得一个群体的认可非常不容，盲目的追求项目的商业化可能会给项目本身带来不可逆的损失。开源贡献者应在合适的时机、合适的条件下去商业化项目。

### 商业化开源项目参考

部分成功的商业化开源项目


```
需补充商业化前的背景，商业化后的发展情况。
```


> Red Hat 

Red Hat Enterprise Linux 是 Red Hat 公司的 Linux 发行版，面向商业市场，包括大型机。

> MySQL 

MySQL 是一个关系型数据库管理系统，由瑞典 MySQL AB 公司开发，属于 Oracle 旗下产品。

> MariaDB 

MariaDB 数据库管理系统是 MySQL 的一个分支，主要由开源社区在维护，采用 GPL 授权许可。MariaDB 的目的是完全兼容 MySQL，包括 API 和命令行，使之能轻松成为 MySQL 的代替品。

> OceanBase 蚂蚁金服数据库

OceanBase 是由蚂蚁集团完全自主研发的金融级分布式关系数据库，始创于 2010 年。OceanBase 具有数据强一致、高可用、高性能、在线扩展、高度兼容 SQL 标准和主流关系数据库、低成本等特点。

> React 

用于构建用户界面的 JavaScript 库

> Vue

是一套构建用户界面的渐进式框架。 

> antd

antd 是基于 Ant Design 设计体系的 React UI 组件库，主要用于研发企业级中后台产品。



还有很多很多开源项目走向了商业化……

